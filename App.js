/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity
} from 'react-native';

import Repo from './components/repo';

export default class App extends Component {
  state = {
    repos: [
      {
        id: 1,
        thumbnail: 'https://avatars1.githubusercontent.com/u/12355312?s=460&v=4',
        title: 'diego.com.br',
        author: 'Diego'
      },
      {
        id: 2,
        thumbnail: 'https://avatars1.githubusercontent.com/u/12355312?s=460&v=4',
        title: 'diego.com.br',
        author: 'Diego'
      }
    ]
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Search Repo</Text>
          <TouchableOpacity onPress={() => {}}>
              <Text style={styles.headerButton}>+</Text>
          </TouchableOpacity>
        </View>
        <ScrollView contentContainerStyle={styles.repoList}>
         { this.state.repos.map(repo => 
         <Repo key={repo.id} data={repo} />) }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333',
  },
  header: {
    height: (Platform.OS === 'ios') ? 70 : 50,
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    flexDirection: 'row',
    backgroundColor: '#FFF',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  headerButton: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  headerText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  repoList: {
    padding: 20,
  },
});
